# README

## First create a python virtual environment

```bash
python3 -m venv demo_venv
```

Activate the python environment

```bash
source demo_venv/bin/activate
```

Verify that you are in the virtual environment

```bash
which pip
$ ~/tiqi-js-tutorial/demo_venv/bin/pip
which python
$ ~/tiqi-js-tutorial/demo_venv/bin/python
```

## Create mock device class

```bash
mkdir device
touch device/__init__.py
```

## Create Flask server

```bash
pip install flask
```

## Create JS Frontend

```bash
mkdir frontend
cd frontend
npm init
```

```bash
npm install parcel-bundler
```

## Serve frontend from flask

```bash
npm run-script build
```

## Use a production-quality WSGI server

```bash
pip install waitress
```

## Running the final repo

To run the final version of the code, clone the repo, and run the following commands from inside the repo folder.
```bash
python3 -m venv demo_venv
source demo_venv/bin/activate
pip install -r requirements.txt
cd frontend
npm install
npm run-script build
cd ..
python3 server.py
```
