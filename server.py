import flask
from device import Device

device = Device()
app = flask.Flask(__name__)


@app.route('/')
def index():
    return main('index.html')


@app.route('/<path:path>')
def main(path):
    return flask.send_from_directory('frontend/build/', path)


@app.route('/get_voltage')
def get_voltage():
    return str(device.voltage)


@app.route('/set_voltage/<new_voltage>')
def set_voltage(new_voltage):
    device.voltage = float(new_voltage)
    return ''


@app.route('/get_current')
def get_current():
    return str(device.current)


@app.route('/set_current/<new_current>')
def set_current(new_current):
    device.current = float(new_current)
    return ''


@app.route('/calculate_resistance')
def resistance():
    return str(device.resistance)


if __name__ == '__main__':
    import waitress
    waitress.serve(app, host='0.0.0.0', port=8000)
