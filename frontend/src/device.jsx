import React from "react";
import axios from "axios";

export default class Device extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      voltage: 0,
      current: 0,
      resistance: 0,
    };

    this.getValues = this.getValues.bind(this);
  }

  async componentDidMount() {
    this.getValues();
  }

  async getValues() {
    const voltage = await axios.get("http://localhost:8000/get_voltage");
    const current = await axios.get("http://localhost:8000/get_current");
    const resistance = await axios.get(
      "http://localhost:8000/calculate_resistance"
    );
    this.setState({
      voltage: voltage.data,
      current: current.data,
      resistance: resistance.data,
    });
  }

  render() {
    return (
      <div class="container mt-5">
        <div class="card">
          <div class="card-body">
            <h3 class="card-title">{this.props.name}</h3>

            <div class="input-group mb-3">
              <label class="input-group-text">Voltage</label>
              <input
                value={this.state.voltage}
                onChange={(event) =>
                  this.setState({ voltage: event.target.value })
                }
                onBlur={async () => {
                  await axios.get(
                    `http://localhost:8000/set_voltage/${this.state.voltage}`
                  );
                  await this.getValues();
                }}
                class="form-control"
              ></input>
            </div>
            <div class="input-group mb-3">
              <label class="input-group-text">Current</label>
              <input
                value={this.state.current}
                onChange={(event) =>
                  this.setState({ current: event.target.value })
                }
                onBlur={async () => {
                  await axios.get(
                    `http://localhost:8000/set_current/${this.state.current}`
                  );
                  await this.getValues();
                }}
                class="form-control"
              ></input>
            </div>
            <div class="input-group mb-3">
              <label class="input-group-text">Resistance</label>
              <input
                value={this.state.resistance}
                readOnly
                class="form-control"
              ></input>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
