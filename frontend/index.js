import "regenerator-runtime/runtime";
import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import Device from "./src/device";

ReactDOM.render(<Device name="My Device" />, document.getElementById("root"));
