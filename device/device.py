class Device:
    __voltage = 0.0
    __current = 0.0

    voltage = None
    current = None

    def __init__(self):
        # code to connect to device
        self.__voltage = 1.0
        self.__current = 0.0

    def _get_voltage(self):
        # read voltage from device
        voltage = self.__voltage
        self.voltage = voltage
        return voltage

    def _set_voltage(self, new_voltage):
        # write voltage to device
        self.__voltage = new_voltage

    @property
    def voltage(self):
        return self._get_voltage()

    @voltage.setter
    def voltage(self, new_voltage):
        self._set_voltage(new_voltage)

    def _get_current(self):
        # read current from device
        current = self.__current
        self.current = current
        return current

    def _set_current(self, new_current):
        # write current to device
        self.__current = new_current

    @property
    def current(self):
        return self._get_current()

    @current.setter
    def current(self, new_current):
        self._set_current(new_current)

    def _calculate_resistance(self):
        if self.current == 0:
            return float('inf')
        current = self.current
        voltage = self.voltage
        resistance = voltage / current
        return resistance

    @property
    def resistance(self):
        return self._calculate_resistance()
